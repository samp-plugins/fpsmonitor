#include <filesystem>
#include <memory>
#include <lemon/hook.hpp>
#include "plugin.hpp"

std::filesystem::path g_config_path;

std::unique_ptr<plugin> plug;

void gameloop() {
	static bool init = false;
	if (init || !dx9::find_device()) return;
	plug = std::make_unique<plugin>();
	init = true;
}

BOOL APIENTRY DllMain(HMODULE hModule, DWORD dwReasonForCall, LPVOID) {
	static lemon::hook<> gameloop_hook(0x748DA3);
	if (dwReasonForCall == DLL_PROCESS_ATTACH) {
		wchar_t mod_path[MAX_PATH] = { 0 };
		GetModuleFileNameW(hModule, mod_path, MAX_PATH);
		g_config_path = std::filesystem::path(mod_path).replace_extension("ini");

		gameloop_hook.on_before += &gameloop;
		gameloop_hook.install();
	}
	return TRUE;
}
