#include "dx9.hpp"
#include <cstring>
#include <string>
#include <windows.h>
#include <lemon/mem.hpp>

IDirect3DDevice9 *dx9::device_ = nullptr;

bool dx9::find_device() {
	if (device_) return true;

	// Locate system d3d9.dll
	std::string d3d9_path(MAX_PATH, '\0');
	auto		size = GetSystemDirectoryA(d3d9_path.data(), MAX_PATH);
	if (!size) return false;
	d3d9_path.resize(size);
	d3d9_path += "\\d3d9.dll";

	// Wait for d3d9.dll load
	auto d3d9_handle = LoadLibraryA(d3d9_path.c_str());
	if (!d3d9_handle) return false;

	auto d3d9_addr	   = reinterpret_cast<std::uintptr_t>(d3d9_handle);
	auto d3d9_ntheader = reinterpret_cast<IMAGE_NT_HEADERS *>(d3d9_addr + reinterpret_cast<IMAGE_DOS_HEADER *>(d3d9_addr)->e_lfanew);
	auto d3d9_size	   = d3d9_ntheader->OptionalHeader.SizeOfImage;

	// C7 06 ?? ?? ?? ?? 89 86 ?? ?? ?? ?? 89 86
	auto mov_pattern = reinterpret_cast<const std::uint8_t *>("\xC7\x06\x00\x00\x00\x00\x89\x86\x00\x00\x00\x00\x89\x86");
	auto mov_mask	 = "xx????xx????xx";
	auto mov_addr	 = lemon::mem::find_pattern(d3d9_addr, d3d9_size, mov_pattern, mov_mask);
	// If offset is not found, then it's not original d3d9.dll
	// Then use game D3D9 device (shown on OBS)
	if (mov_addr) {
		device_ = reinterpret_cast<IDirect3DDevice9 *>(mov_addr + 0x2);
	} else {
		device_ = *reinterpret_cast<IDirect3DDevice9 **>(device_ptr_addr);
	}
	return device_ != nullptr;
}

IDirect3DDevice9 *dx9::device() {
	return device_;
}

IDirect3DDevice9 *dx9::game_device() {
	return *reinterpret_cast<IDirect3DDevice9 **>(device_ptr_addr);
}

std::uintptr_t dx9::vt_func_addr(std::uint32_t offset) {
	if (!device()) return 0;
	return (*reinterpret_cast<std::uintptr_t **>(device()))[offset];
}