#include "plugin.hpp"
#include <filesystem>
#include <fstream>
#include <regex>
#include <thread>
#include <backends/imgui_impl_dx9.h>
#include <backends/imgui_impl_win32.h>
#include <imgui.h>
#include "ssnr.hpp"

extern std::filesystem::path g_config_path;

enum fpspos_flags : int { right	   = 1,
						  bottom   = 2,
						  disabled = 4 };

plugin::plugin() {
	read_config();

	// Find SAMP library in memory
	auto samp_addr = reinterpret_cast<std::uintptr_t>(GetModuleHandleA("samp.dll"));
	auto samp_size = reinterpret_cast<IMAGE_NT_HEADERS *>(samp_addr + reinterpret_cast<IMAGE_DOS_HEADER *>(samp_addr)->e_lfanew)->OptionalHeader.SizeOfImage;

	// Init ImGui
	ImGui::CreateContext();
	ImGui_ImplWin32_Init(**reinterpret_cast<HWND **>(0xC17054));
	ImGui_ImplDX9_Init(dx9::game_device());
	ImFontConfig font_config;
	font_config.GlyphExtraSpacing.x = -5.f;
	auto &io						= ImGui::GetIO();
	io.Fonts->AddFontFromMemoryCompressedBase85TTF(ssnr_compressed_data_base85, 23.5f, &font_config);
	io.IniFilename = nullptr;

	// DX9 hooks
	auto present_addr	= present_hook_.addr();
	auto present_hooked = (*reinterpret_cast<std::uint8_t *>(present_addr) & 0xF0) == 0xE0;
	if (present_hooked) present_hook_.set_addr(present_addr + 0x5);
	present_hook_.on_before += std::make_tuple(this, &plugin::on_present);
	present_hook_.install(present_hooked ? 8 : 4);

	auto reset_addr	  = reset_hook_.addr();
	auto reset_hooked = (*reinterpret_cast<std::uint8_t *>(reset_addr) & 0xF0) == 0xE0;
	if (reset_hooked) reset_hook_.set_addr(reset_addr + 0x5);
	reset_hook_.on_before += std::make_tuple(this, &plugin::on_reset);
	reset_hook_.install(reset_hooked ? 8 : 4);

	// Find cmdFpsLimit address (needed for add info to chat function)
	constexpr auto cmd_fpslimit_bytes = "\x83\xFE\x14\x72";
	constexpr auto cmd_fpslimit_mask  = "xxxx";
	cmd_fpslimit_addr_				  = lemon::mem::find_pattern(samp_addr, samp_size, cmd_fpslimit_bytes, cmd_fpslimit_mask);

	using namespace Xbyak::util;
	// Save EAX, return address, ECX and EDX
	fpspos_mem_->mov(ptr[&fpspos_context_.EAX], eax);
	fpspos_mem_->pop(eax);
	fpspos_mem_->mov(ptr[&fpspos_context_.ret_addr], eax);
	fpspos_mem_->mov(ptr[&fpspos_context_.ECX], ecx);
	fpspos_mem_->mov(ptr[&fpspos_context_.EDX], edx);
	// Repush cmd params
	fpspos_mem_->mov(edx, ptr[esp]);
	fpspos_mem_->push(edx);
	// Call cmd handler
	fpspos_mem_->mov(ecx, reinterpret_cast<std::uintptr_t>(this));
	auto cmd_handler_method = &plugin::cmd_fpspos;
	fpspos_mem_->call(reinterpret_cast<void *&>(cmd_handler_method));
	// Restore EAX, return address, ECX and EDX
	fpspos_mem_->mov(edx, ptr[&fpspos_context_.EDX]);
	fpspos_mem_->mov(ecx, ptr[&fpspos_context_.ECX]);
	fpspos_mem_->mov(eax, ptr[&fpspos_context_.ret_addr]);
	fpspos_mem_->push(eax);
	fpspos_mem_->mov(eax, ptr[&fpspos_context_.EAX]);
	// Return
	fpspos_mem_->ret();

	// Install add SAMP command hook (for own command registration)
	constexpr auto add_cmd_bytes = "\x57\x8B\xB9\x00\x00\x00\x00\x81\xFF\x90\x00\x00\x00\x7D";
	constexpr auto add_cmd_mask	 = "xxx????xxxxxxx";
	add_cmd_addr_				 = lemon::mem::find_pattern(samp_addr, samp_size, reinterpret_cast<const std::uint8_t *>(add_cmd_bytes), add_cmd_mask);
	add_cmd_hook_.set_addr(add_cmd_addr_);
	add_cmd_hook_.install(std::make_tuple(this, &plugin::add_cmd));
}

plugin::~plugin() {
	// Clear command memory
	auto fpspos_mem_size = fpspos_mem_->getSize();
	if (fpspos_mem_size > 0) {
		fpspos_mem_->resetSize();
		fpspos_mem_->ret();
		fpspos_mem_->nop(fpspos_mem_size - 1);
		fpspos_mem_ = nullptr;
	}

	// Remove DX9 hooks
	present_hook_.remove();
	reset_hook_.remove();

	// Shutdown ImGui
	ImGui_ImplDX9_Shutdown();
	ImGui_ImplWin32_Shutdown();
	ImGui::DestroyContext();
}

void plugin::on_present() {
	if (fpspos_ & fpspos_flags::disabled) return;

	ImGui_ImplDX9_NewFrame();
	ImGui_ImplWin32_NewFrame();
	ImGui::NewFrame();

	static std::string							 fps_text;
	static std::chrono::steady_clock::time_point time_point;

	auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - time_point).count();
	if (elapsed >= 500) {
		fps_text   = std::to_string(static_cast<int>(ImGui::GetIO().Framerate));
		time_point = std::chrono::steady_clock::now();
	}

	auto [width, height] = screen_res();
	auto fps_text_size	 = ImGui::CalcTextSize(fps_text.c_str());
	auto fps_text_pos	 = ImVec2(5.f, 5.f);
	if (fpspos_ & fpspos_flags::right) fps_text_pos.x = width - fps_text_size.x - 5.f;
	if (fpspos_ & fpspos_flags::bottom) fps_text_pos.y = height - fps_text_size.y - 5.f;

	auto draw_list = ImGui::GetBackgroundDrawList();
	draw_list->AddText(fps_text_pos, 0xFF00B976, fps_text.c_str());

	ImGui::EndFrame();
	ImGui::Render();
	ImGui_ImplDX9_RenderDrawData(ImGui::GetDrawData());
}

void plugin::on_reset() {
	ImGui_ImplDX9_InvalidateDeviceObjects();
}

std::tuple<float, float> plugin::screen_res() {
	static constexpr auto screen_res_addr = 0xC9C040u;
	const auto			  width			  = *reinterpret_cast<int *>(screen_res_addr);
	const auto			  height		  = *reinterpret_cast<int *>(screen_res_addr + 0x4);
	return std::make_tuple(static_cast<const float>(width), static_cast<const float>(height));
}

void plugin::read_config() {
	static std::regex re(R"(^fpspos\s*=\s*(\d+)$)", std::regex::icase);

	std::ifstream config_file(g_config_path);
	if (!config_file.is_open()) return save_config();

	bool		read = false;
	std::smatch sm;
	std::string line;
	while (!read && std::getline(config_file, line)) {
		if (std::regex_match(line, sm, re)) {
			fpspos_ = std::stoi(sm[1].str()) % 5;
			read	= true;
		}
	}
	if (!read) save_config();
}

void plugin::save_config() {
	std::ofstream config_file(g_config_path);
	if (!config_file.is_open()) return;
	config_file << "fpspos = " << fpspos_ << std::endl;
}

void plugin::add_cmd(add_cmd_t orig, void *this_, const char *name, cmd_t func) {
	orig(this_, "fpspos", fpspos_mem_->getCode<cmd_t>());

	orig(this_, name, func);
	add_cmd_hook_.remove();
}

void plugin::cmd_fpspos(const char *params) {
	using add_info_msg_t	 = void(__cdecl *)(void *, const char *, ...);
	static auto chat_ptr	 = **reinterpret_cast<void ***>(cmd_fpslimit_addr_ + 0x2D + 1);
	static auto add_info_msg = *reinterpret_cast<std::uintptr_t *>(cmd_fpslimit_addr_ + 0x39 + 1) + cmd_fpslimit_addr_ + 0x39 + 5;

	fpspos_ = (fpspos_ + 1) % 5;
	save_config();
	if (fpspos_ & fpspos_flags::disabled) {
		reinterpret_cast<add_info_msg_t>(add_info_msg)(chat_ptr, "-> FPS Monitor position was disabled");
	} else {
		reinterpret_cast<add_info_msg_t>(add_info_msg)(chat_ptr, "-> FPS Monitor position was updated");
	}
}