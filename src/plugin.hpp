#ifndef PLUGIN_HPP
#define PLUGIN_HPP

#include <tuple>
#include <vector>
#include <lemon/detour.hpp>
#include <lemon/hook.hpp>
#include "dx9.hpp"

using cmd_t		= void(__cdecl *)(const char *);
using add_cmd_t = void(__thiscall *)(void *, const char *, cmd_t);

struct cmd_context {
	std::uint32_t EAX;
	std::uint32_t ECX;
	std::uint32_t EDX;

	std::uint32_t ret_addr;
};

class plugin {
	// [FPS monitor related]
	lemon::hook<> present_hook_{ dx9::vt_func_addr(17) };
	lemon::hook<> reset_hook_{ dx9::vt_func_addr(16) };

	std::uintptr_t cmd_fpslimit_addr_ = 0;

	int					  fpspos_	  = 0;
	Xbyak::CodeGenerator *fpspos_mem_ = new Xbyak::CodeGenerator();
	struct cmd_context	  fpspos_context_;

	std::uintptr_t			 add_cmd_addr_ = 0;
	lemon::detour<add_cmd_t> add_cmd_hook_{ 0x0 };

public:
	plugin();
	~plugin();

private:
	void							on_present();
	void							on_reset();
	static std::tuple<float, float> screen_res();

	void read_config();
	void save_config();

	void add_cmd(add_cmd_t orig, void *this_, const char *name, cmd_t func);
	void cmd_fpspos(const char *params);
};

#endif // PLUGIN_HPP
